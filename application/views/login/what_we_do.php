<div class="image-aboutus-banner" style="margin-top:55px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="lg-text">About Us</h1>
                <p class="image-aboutus-para">MMU Happening. A place for events in MMU Cyberjaya.</p>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid" style="margin-top:25px;">
    <div class="aboutus-secktion paddingTB60">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="strong">Who we are and<br>what we do</h1>
                    <p class="lead">This is the best place for you to share your event </p>
                </div>
                <div class="col-md-6">
                    <p>
                        <b>MMU Happening!</b> is a place where you can promote your event to MMU Cyberjaya Students
                    </p>
                    <p>Just <a href="<?php echo site_url('login/register'); ?>" style="color:blue;">Register</a> and become an applicant to post your Events in MMU Happening!</p>
                </div>
            </div>
        </div>
    </div>
</div>
<hr />

<div class="container" style="margin:auto;">
    <div class="row m-4">
        <div class="col-lg-12 text-center">
            <a href="<?php echo site_url('login/register'); ?>" class="btn btn-primary btn-lg">>> Become an applicant <<</a>
            <br /><br />
            <a href="<?php echo site_url('login/event/all'); ?>" class="btn btn-primary btn-lg">>> View more events here <<</a>
        </div>
    </div>
</div>