<div class="container mt-5 mb-5 pt-5 pb-5">
    <div class="row py-6 mt-5 align-items-center ">
        <div class="col-md-5 pr-lg-5 mb-5 mb-md-0">
            <img src="https://res.cloudinary.com/mhmd/image/upload/v1569543678/form_d9sh6m.svg" alt="" class="img-fluid mb-3 d-none d-md-block">
        </div>

        <div class="col-md-7 col-lg-6 ml-auto">
            <form method="post">
                <?php
                    $flash = $this->session->flashdata('message');
                    if (!empty($flash)) :
                ?>
                    <div class="alert alert-<?php echo $flash['type']; ?>"><?php echo $flash['message']; ?></div>
                <?php endif; ?>
                <div class="row">
                    <label class="col-lg-11 mb-4" for="username">Enter your username and we will send you an email: </label>
                    <div class="form-group col-lg-12 <?php echo form_has_error('username'); ?>">
                        <div class="input-group mb-4">
                            <input type="text" class="form-control bg-white border-left-1 border-md <?php echo form_has_error('username'); ?>" id="username" name="username" placeholder="Enter your username..." value="<?php echo set_value('username'); ?>" />
                        </div>
                        <?php echo form_error_label('username'); ?>
                    </div>

                    <div class="form-group col-lg-12 mx-auto mb-0">
                        <button type="submit" class="btn btn-primary btn-block py-2">
                            <span class="">Reset Password</span>
                        </button>
                    </div>

                    <div class="form-group col-lg-12 mx-auto d-flex align-items-center my-4">
                        <div class="border-bottom w-100 ml-5"></div>
                        <span class="px-2 small text-muted font-weight-bold text-muted">OR</span>
                        <div class="border-bottom w-100 mr-5"></div>
                    </div>

                    <div class="text-center w-100">
                        <p class="text-muted "><a href="<?php echo site_url('login/login'); ?>" class="text-primary ml-2">Return to Login</a></p>
                    </div>
                    <div class="text-center w-100">
                        <a href="<?php echo site_url('login/register'); ?>" class="text-primary  ml-2">Need an account? Sign up!</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>