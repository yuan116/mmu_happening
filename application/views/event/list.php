<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Event</h1>

            <div class="card mb-4">
                <div class="card-header"><i class="fas fa-table mr-1"></i>Event Table</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="event_dtb" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Type</th>
                                    <th>Title</th>
                                    <th>Venue</th>
                                    <th>Status</th>
                                    <th>Created By</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php $this->load->view('components/html_footer'); ?>
</div>

<script>
    var event_dtb;
    $(document).ready(function() {
        event_dtb = $('#event_dtb').DataTable({
            "ajax"       : '<?php echo site_url('event/get_event_list'); ?>',
            "columns"    : [
                {data : null},
                {data : 'type',       name : 'type'},
                {data : 'title',      name : 'title'},
                {data : 'venue',      name : 'venue'},
                {data : 'status',     name : 'status'},
                {data : 'fullname',   name : 'fullname'},
                {data : 'created_at', name : 'created_at'},
                {data : null}
            ],
            "order"      : [[4, 'asc']],
            "columnDefs" : [
                {
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 0
                },

                {
                    "visible" : <?php echo ($this->session->userdata('role_keyword') == ROLE_ADMIN) ? 'true' : 'false'; ?>,
                    "targets" : 5
                },

                {
                    'render': function(data, type, row) {
                        var btn = '';

                        if(row['status'] == 'Pending'){
                            btn += '<a href="<?php echo site_url('event/view'); ?>' + '/' + row['id'] + '" class="btn btn-sm btn-info btn-flat" title="View"><i class="fa fa-eye fa-fw"></i></a>&nbsp;';
                        }

                        if(row['status'] == 'Approved' && '<?php echo $this->session->userdata('user_id') ?>' == row['created_by']){
                            btn += '<a href="<?php echo site_url('event/form'); ?>' + '/' + row['id'] + '" class="btn btn-sm btn-primary btn-flat" title="Edit"><i class="fa fa-edit fa-fw"></i></a>&nbsp;';
                        }

                        if(row['status'] == 'Pending' && '<?php echo $this->session->userdata('user_id') ?>' == row['created_by']){
                            btn += '<a href="<?php echo site_url('event/delete'); ?>' + '/' + row['id'] + '" class="btn btn-sm btn-danger btn-flat" title="Delete" onclick="return window.confirm(\'Are you sure to delete. Once delete cannot recover back.\');"><i class="fa fa-trash fa-fw"></i></a>';
                        }

                        <?php if($this->session->userdata('role_keyword') == ROLE_ADMIN): ?>
                            if(row['status'] == 'Pending'){
                                btn += '<a href="<?php echo site_url('event/action/A'); ?>/' + row['id'] + '" class="btn btn-sm btn-success btn-flat" title="Approve"><i class="fa fa-check fa-fw"></i></a>&nbsp;';
                                btn += '<a href="<?php echo site_url('event/action/R'); ?>/' + row['id'] + '" class="btn btn-sm btn-danger btn-flat" title="Reject"><i class="fa fa-times fa-fw"></i></a>';
                            }
                        <?php endif; ?>

                        return btn;
                    },
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 7
                },
            ]
        });

        event_dtb.on('order.dt search.dt', function () {
            event_dtb.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });
</script>