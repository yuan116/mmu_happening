<?php

/**
 * @author Lim Chun Yuan <lim116@yahoo.com>
 * @version 1.0.0
 * @since 15-FEB-2020 - 16-FEB-2020
 */

class MY_Model extends CI_Model
{
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $_table_name;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $_primary_key;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $_order_by;

    protected $_created_by = 'created_by';
    protected $_created_date = 'created_at';

    protected $_updated_by = 'updated_by';
    protected $_updated_date = 'updated_at';

    protected $_deleted_by = 'deleted_by';
    protected $_deleted_date = 'deleted_at';

    public function __construct()
    {
        parent::__construct();
    }

    public function set_property($properties, string $data = NULL): void
    {
        if (is_array($properties))
        {
            foreach($properties as $key => $value)
            {
                $this->{"_{$key}"} = $value;
            }
        }
        else
        {
            $this->{"_{$properties}"} = $data;
        }
    }

    public function get_data(string $select = '*', array $where = array(), bool $single_row = FALSE, string $type = 'object')
    {
        return $this->_run_query($select, $where, $single_row, $type);
    }

    public function get_data_by_key(string $select = '*', $id, bool $single_row = FALSE, string $type = 'object')
    {
        return $this->_run_query($select, $id, $single_row, $type);
    }

    public function get_data_from(string $select = '*', string $from = NULL, array $where = array(), bool $single_row = FALSE, string $type = 'object')
    {
        $this->set_property('table_name', $from);
        return $this->_run_query($select, $where, $single_row, $type);
    }

    private function _run_query(string $select, $where, bool $single_row, string $type)
    {
        $this->db->select($select);

        if ( ! empty($where))
        {
            if( is_array($where))
            {
                $this->db->where($where);
            }
            else
            {
                $this->db->where($this->_primary_key, $where);
            }
        }

        if ( ! empty($this->_order_by))
        {
            $this->db->order_by($this->_order_by);
        }

        $db_result = $this->db->get($this->_table_name);

        if ($single_row === TRUE)
        {
            return $db_result->row(0, $type);
        }
        else
        {
            return $db_result->result($type);
        }
    }

    public function save(array $data, $id = NULL, $is_delete = FALSE)
    {
        if (empty($id))
        {
            $this->_set_save_value('created', $data);

            $this->db->insert($this->_table_name, $data);
            return $this->db->insert_id();
        }
        else
        {
            if ($is_delete === FALSE)
            {
                $this->_set_save_value('updated', $data);
            }
            else
            {
                $this->_set_save_value('deleted', $data);
            }

            return $this->db->update($this->_table_name, $data, array($this->_primary_key => $id));
        }
    }

    private function _set_save_value(string $type, array &$data)
    {
        if ($this->db->field_exists($this->{"_{$type}_by"}, $this->_table_name))
        {
            $data[$this->{"_{$type}_by"}] = $this->session->userdata('user_id');
            $data[$this->{"_{$type}_date"}] = date('Y-m-d H:i:s');
        }
    }

    public function delete($id, $delete = FALSE): bool
    {
        if ($delete === TRUE || ($this->db->field_exists('deleted', $this->_table_name) === FALSE))
        {
            return $this->db->delete($this->_table_name, array($this->_primary_key => $id));
        }
        else
        {
            return $this->save(array('deleted' => TRUE), $id, TRUE);
        }
    }

    public function my_dtb($obj, bool $by_having = FALSE): void
    {
        $offset      = $obj['start'];
        $limit       = $obj['length'];
        $columns     = $obj['columns'];
        $orders      = isset($obj['order']) ? $obj['order'] : array();
        $search      = $obj['search'];
        $group_start = FALSE;

        foreach ($columns as $column)
        {
            if ( ! empty($column['name']))
            {
                if ( ! empty($search['value']))
                {
                    if ($by_having === TRUE)
                    {
                        $this->db->or_having("{$column['name']} LIKE '%{$search['value']}%'");
                    }
                    else
                    {
                        if ($group_start === FALSE)
                        {
                            $this->db->group_start();
                            $group_start = TRUE;
                        }

                        $this->db->or_like($column['name'], $search['value']);
                    }
                }

                if ( ! empty($column['search']['value']))
                {
                    if ($by_having === TRUE)
                    {
                        $this->db->or_having("{$column['name']} LIKE '%{$column['search']['value']}%'");
                    }
                    else
                    {
                        if ($group_start === FALSE)
                        {
                            $this->db->group_start();
                            $group_start = TRUE;
                        }

                        $this->db->or_like($column['name'], $column['search']['value']);
                    }
                }
            }
        }

        if ($group_start === TRUE)
        {
            $this->db->group_end();
        }

        if ( ! empty($orders))
        {
            foreach ($orders as $order)
            {
                if ( ! empty($columns[$order['column']]['name']))
                {
                    $this->db->order_by($columns[$order['column']]['name'], $order['dir']);
                }
            }
        }

        $this->db->limit($limit, $offset);
    }

    public function my_dtb_count($obj, bool $by_having = FALSE): void
    {
        $columns     = $obj['columns'];
        $search      = $obj['search'];
        $group_start = FALSE;

        foreach ($columns as $column)
        {
            if ( ! empty($column['name']))
            {
                if ( ! empty($search['value']))
                {
                    if ($by_having === TRUE)
                    {
                        $this->db->or_having("{$column['name']} LIKE '%{$search['value']}%'");
                    }
                    else
                    {
                        if ($group_start === FALSE) {
                            $this->db->group_start();
                            $group_start = TRUE;
                        }

                        $this->db->or_like($column['name'], $search['value']);
                    }
                }

                if ( ! empty($column['search']['value']))
                {
                    if ($by_having === TRUE)
                    {
                        $this->db->or_having("{$column['name']} LIKE '%{$column['search']['value']}%'");
                    }
                    else
                    {
                        if ($group_start === FALSE)
                        {
                            $this->db->group_start();
                            $group_start = TRUE;
                        }

                        $this->db->or_like($column['name'], $column['search']['value']);
                    }
                }
            }
        }

        if ($group_start === TRUE)
        {
            $this->db->group_end();
        }
    }
}
