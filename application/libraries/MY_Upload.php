<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Lim Chun Yuan <lim116@yahoo.com>
 * @version 1.0.0
 * @since 15-FEB-2020 - 15-FEB-2020
 */

class MY_Upload extends CI_Upload
{
    /**
     * Undocumented variable
     *
     * @var array
     */
    private $_config = array(
        'upload_path'            => 'uploads/',
        'allowed_types'          => NULL,
        'file_name'              => NULL,
        'file_ext_tolower'       => TRUE,
        'overwrite'              => FALSE,
        'max_size'               => 0,
        'max_width'              => 0,
        'max_height'             => 0,
        'min_width'              => 0,
        'min_height'             => 0,
        'max_filename'           => 0,
        'max_filename_increment' => 100,
        'encrypt_name'           => FALSE,
        'remove_spaces'          => TRUE,
        'detect_mime'            => TRUE,
        'mod_mime_fix'           => TRUE
    );

    public function __construct(array $config = array())
    {
        if ( ! empty($config))
        {
            foreach ($config as $key => $value)
            {
                $this->_config[$key] = $value;
            }
        }

        parent::__construct($this->_config);
    }
}